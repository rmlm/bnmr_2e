
// g++ -std=c++11 -o bnmr_2e bnmr_2e.cpp `root-config --cflags --glibs` -lmud -Wall

#include <mud.h>

#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <functional>
#include <iomanip>
#include <iostream>
#include <limits>
#include <numeric>
#include <string>
#include <vector>

#include <TApplication.h>
#include <TCanvas.h>
#include <TF1.h>
#include <TH1.h>
#include <TH1D.h>
#include <TH2.h>
#include <THStack.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TLegend.h>
#include <TMath.h>
#include <TMultiGraph.h>
#include <TQObject.h>
#include <TStyle.h>

// Get MUD data from the web.
void wgetMUD(unsigned long year, unsigned long run);
// Set ROOT asthetics
void SetROOTStyle();

int main(int argc, char **argv)
{
   SetROOTStyle();
   TApplication theApp("theApp", &argc, argv);
   //std::cout << argc << "\n";
   if ( argc != 3 )
   {
      std::cout << "Invalid input: must be of the form: ./bnmr_2e <year> <run>\n";
      return EXIT_FAILURE;
   }
   
   unsigned long year = std::strtoul(argv[1], nullptr, 10);
   unsigned long run = std::strtoul(argv[2], nullptr, 10);
   if (year < 2005)
   {
      std::cout << "Invalid input: <year> must be 2005 or later.\n" 
                << "               2e mode wasn't implemented until then!" << std::endl;
      return EXIT_FAILURE;
   }
   if (run<40000 or run>50000)
   {
      std::cout << "Invalid input: <run> must be between 40000 and 50000." << std::endl;
      return EXIT_FAILURE;
   }
   
   
   //unsigned int rebin = 1;
   //if (argc == 4)
   //{
   //   rebin = static_cast<unsigned int>( std::atoi(argv[3]) );
   //}
   //std::cout << "rebin = " << rebin << std::endl;
   
   //wgetMUD(year, run);

   //std::string path("/home/bnmr/data1/");
   std::string path("/home/rmlm/Documents/triumf/data/");
   if ( run >= 40000 and run < 45000 ) path.append("bnmr/" + std::to_string(year) + "/");
   if ( run >= 45000 and run < 50000 ) path.append("bnqr/" + std::to_string(year) + "/");
   std::string localfile(path);
   localfile.append( "0" + std::to_string(run) + ".msr");
   //std::string cleanup("rm -f " + localfile);
   
   
   int fHandle;
   unsigned int pType;
   unsigned int pNumber;
   fHandle = MUD_openRead( const_cast<char*>(localfile.c_str()) , &pType);
   
   char mode[128];
   MUD_getRunDesc(fHandle, &pType);
   MUD_getInsert(fHandle, mode, 128);
   std::cout << "This is a " << mode << " run - ";
   if ( strcmp(mode,"2e") == 0 or strcmp(mode,"2E") == 0 )
   {
      std::cout << "continuing.\n";
   }
   else
   {
      std::cout << "exiting.\n";
      MUD_closeRead(fHandle);
      //std::system( cleanup.c_str() );
      return EXIT_FAILURE;
   }
   
   /////////////////////////////////////////////////////////////////////////////////////////////
   // 2e                                                                                      //
   /////////////////////////////////////////////////////////////////////////////////////////////
   
	double PPG_2e_Mode;
	double PPG_2e_ModeError; 
	double PPG_2e_ModeSkewness;
	double PPG_2e_ModeMin;
	double PPG_2e_ModeMax;
	
	double PPG_2e_InitMode;
	double PPG_2e_InitModeError;
	double PPG_2e_InitModeSkewness;
	double PPG_2e_InitModeMin;
	double PPG_2e_InitModeMax;
	
	double PPG_2e_FrequencyStart = std::numeric_limits<double>::quiet_NaN();
	double PPG_2e_FrequencyStartError;
	double PPG_2e_FrequencyStartSkewness;
	double PPG_2e_FrequencyStartMin;
	double PPG_2e_FrequencyStartMax;
	// "PPG/PPG2e/frequency stop (Hz)"
	double PPG_2e_FrequencyStop = std::numeric_limits<double>::quiet_NaN();
	double PPG_2e_FrequencyStopError;
	double PPG_2e_FrequencyStopSkewness;
	double PPG_2e_FrequencyStopMin;
	double PPG_2e_FrequencyStopMax;
	// "PPG/PPG2e/frequency increment (Hz)"
	double PPG_2e_FrequencyIncrement = std::numeric_limits<double>::quiet_NaN();	
	double PPG_2e_FrequencyIncrementError;
	double PPG_2e_FrequencyIncrementSkewness;
	double PPG_2e_FrequencyIncrementMin;
	double PPG_2e_FrequencyIncrementMax;
	// "PPG/PPG2e/Randomize freq values"
	double PPG_2e_FrequencyRandomize;	
	double PPG_2e_FrequencyRandomizeError;
	double PPG_2e_FrequencyRandomizeSkewness;
	double PPG_2e_FrequencyRandomizeMin;
	double PPG_2e_FrequencyRandomizeMax;
   // "PPG/PPG2e/RF on time (ms)" - this is equivalent to a slr(20) dwelltime
	double PPG_2e_RFOnTime;
	double PPG_2e_RFOnTimeError;
	double PPG_2e_RFOnTimeSkewness;
	double PPG_2e_RFOnTimeMin;
	double PPG_2e_RFOnTimeMax;
	// "PPG/PPG2e/Ref tuning freq (Hz)"
	double PPG_2e_RFTuningFrequency; 
	double PPG_2e_RFTuningFrequencyError;
	double PPG_2e_RFTuningFrequencySkewness;
	double PPG_2e_RFTuningFrequencyMin;
	double PPG_2e_RFTuningFrequencyMax;
	// "PPG/PPG2e/num RF on delays (dwell times)"
	double PPG_2e_RFOnDelays = std::numeric_limits<double>::quiet_NaN(); 
	double PPG_2e_RFOnDelaysError;
	double PPG_2e_RFOnDelaysSkewness;
	double PPG_2e_RFOnDelaysMin;
	double PPG_2e_RFOnDelaysMax;
	// "PPG/PPG2e/beam off time (ms)"
	double PPG_2e_BeamOffTime = std::numeric_limits<double>::quiet_NaN(); 
	double PPG_2e_BeamOffTimeError;
	double PPG_2e_BeamOffTimeSkewness;
	double PPG_2e_BeamOffTimeMin;
	double PPG_2e_BeamOffTimeMax;
	//	"PPG/PPG2e/Number dwelltimes per freq"
	double PPG_2e_DwelltimesPerFrequency = std::numeric_limits<double>::quiet_NaN();			
	double PPG_2e_DwelltimesPerFrequencyError;
	double PPG_2e_DwelltimesPerFrequencySkewness;
	double PPG_2e_DwelltimesPerFrequencyMin;
	double PPG_2e_DwelltimesPerFrequencyMax;
	
	double PPG_2e_PostRFBeamOnDwelltimes = std::numeric_limits<double>::quiet_NaN();
	double PPG_2e_PostRFBeamOnDwelltimesError;
	double PPG_2e_PostRFBeamOnDwelltimesSkewness;
	double PPG_2e_PostRFBeamOnDwelltimesMin;
	double PPG_2e_PostRFBeamOnDwelltimesMax;
	
	double PPG_2e_PSMScaleFactor;
	double PPG_2e_PSMScaleFactorError;
	double PPG_2e_PSMScaleFactorSkewness;
	double PPG_2e_PSMScaleFactorMin;
	double PPG_2e_PSMScaleFactorMax;
	
	double PPG_2e_RFEnable;
	double PPG_2e_RFEnableError;
	double PPG_2e_RFEnableSkewness;
	double PPG_2e_RFEnableMin;
	double PPG_2e_RFEnableMax;
	
	double PPG_2e_SingleToneSimulated;	
	double PPG_2e_SingleToneSimulatedError;
	double PPG_2e_SingleToneSimulatedSkewness;
	double PPG_2e_SingleToneSimulatedMin;
	double PPG_2e_SingleToneSimulatedMax;
	
	double PPG_2e_HelicityFlipEnable;
	double PPG_2e_HelicityFlipEnableError;
	double PPG_2e_HelicityFlipEnableSkewness;
	double PPG_2e_HelicityFlipEnableMin;
	double PPG_2e_HelicityFlipEnableMax;
	
	double PPG_2e_HelicityFlipSleep;
	double PPG_2e_HelicityFlipSleepError;
	double PPG_2e_HelicityFlipSleepSkewness;
	double PPG_2e_HelicityFlipSleepMin;
	double PPG_2e_HelicityFlipSleepMax;
   
   
   MUD_getIndVars(fHandle, &pType, &pNumber);
   // Loop through all the logged variables
	for (unsigned int i = 1; i <= pNumber; i++)
	{
		char Name[128];
		char Description[128];
		char Units[128];
		
		MUD_getIndVarName(fHandle, i, Name, 128);
		MUD_getIndVarDescription(fHandle, i, Description, 128);
		MUD_getIndVarUnits(fHandle, i, Units, 128);
   
	   // PPG MODE
    	if ( strcmp(Name,"PPG/PPG2e/PPG mode") == 0 )
    	{
    		MUD_getIndVarMean(fHandle, i, &PPG_2e_Mode);
    		MUD_getIndVarStddev(fHandle, i, &PPG_2e_ModeError);
    		MUD_getIndVarSkewness(fHandle, i, &PPG_2e_ModeSkewness);
    		MUD_getIndVarHigh(fHandle, i, &PPG_2e_ModeMax);
    		MUD_getIndVarLow(fHandle, i, &PPG_2e_ModeMin);
    	} // PPG MODE
    	
    	// PPG INIT MODE
    	if ( strcmp(Name,"PPG/PPG2e/PPG init mode") == 0 or strcmp(Name,"PPG/PPG2e/init mode file") == 0 )
    	{
    		MUD_getIndVarMean(fHandle, i, &PPG_2e_InitMode);
    		MUD_getIndVarStddev(fHandle, i, &PPG_2e_InitModeError);
    		MUD_getIndVarSkewness(fHandle, i, &PPG_2e_InitModeSkewness);
    		MUD_getIndVarHigh(fHandle, i, &PPG_2e_InitModeMax);
    		MUD_getIndVarLow(fHandle, i, &PPG_2e_InitModeMin);
    	} // PPG INIT MODE
			
			// FREQUENCY START
    	if ( strcmp(Name,"PPG/PPG2e/frequency start (Hz)") == 0 )
    	{
    		MUD_getIndVarMean(fHandle, i, &PPG_2e_FrequencyStart);
    		MUD_getIndVarStddev(fHandle, i, &PPG_2e_FrequencyStartError);
    		MUD_getIndVarSkewness(fHandle, i, &PPG_2e_FrequencyStartSkewness);
    		MUD_getIndVarHigh(fHandle, i, &PPG_2e_FrequencyStartMax);
    		MUD_getIndVarLow(fHandle, i, &PPG_2e_FrequencyStartMin);
    	} // FREQUENCY START
			
			// FREQUENCY STOP
    	if ( strcmp(Name,"PPG/PPG2e/frequency stop (Hz)") == 0 )
    	{
    		MUD_getIndVarMean(fHandle, i, &PPG_2e_FrequencyStop);
    		MUD_getIndVarStddev(fHandle, i, &PPG_2e_FrequencyStopError);
    		MUD_getIndVarSkewness(fHandle, i, &PPG_2e_FrequencyStopSkewness);
    		MUD_getIndVarHigh(fHandle, i, &PPG_2e_FrequencyStopMax);
    		MUD_getIndVarLow(fHandle, i, &PPG_2e_FrequencyStopMin);
    	} // FREQUENCY STOP
			
			// FREQUENCY INCREMENT/STEP
    	if ( strcmp(Name,"PPG/PPG2e/frequency increment (Hz)") == 0 )
    	{
    		MUD_getIndVarMean(fHandle, i, &PPG_2e_FrequencyIncrement);
    		MUD_getIndVarStddev(fHandle, i, &PPG_2e_FrequencyIncrementError);
    		MUD_getIndVarSkewness(fHandle, i, &PPG_2e_FrequencyIncrementSkewness);
    		MUD_getIndVarHigh(fHandle, i, &PPG_2e_FrequencyIncrementMax);
    		MUD_getIndVarLow(fHandle, i, &PPG_2e_FrequencyIncrementMin);
    	} // FREQUENCY INCREMENT/STEP
			
			// FREQUENCY RANDOMIZATION
    	if ( strcmp(Name,"PPG/PPG2e/Randomize freq values") == 0 )
    	{
    		MUD_getIndVarMean(fHandle, i, &PPG_2e_FrequencyRandomize);
    		MUD_getIndVarStddev(fHandle, i, &PPG_2e_FrequencyRandomizeError);
    		MUD_getIndVarSkewness(fHandle, i, &PPG_2e_FrequencyRandomizeSkewness);
    		MUD_getIndVarHigh(fHandle, i, &PPG_2e_FrequencyRandomizeMax);
    		MUD_getIndVarLow(fHandle, i, &PPG_2e_FrequencyRandomizeMin);
    	} // RANDOMIZE RANDOMIZATION
			
			// RF ON TIME [ms] - this is equivalent to a slr(20) dwelltime
    	if ( strcmp(Name,"PPG/PPG2e/RF on time (ms)") == 0 )
    	{
    		MUD_getIndVarMean(fHandle, i, &PPG_2e_RFOnTime);
    		MUD_getIndVarStddev(fHandle, i, &PPG_2e_RFOnTimeError);
    		MUD_getIndVarSkewness(fHandle, i, &PPG_2e_RFOnTimeSkewness);
    		MUD_getIndVarHigh(fHandle, i, &PPG_2e_RFOnTimeMax);
    		MUD_getIndVarLow(fHandle, i, &PPG_2e_RFOnTimeMin);
    	} // RF ON TIME [ms]
			
			// RF TUNING FREQUENCY
    	if ( strcmp(Name,"PPG/PPG2e/Ref tuning freq (Hz)") == 0 )
    	{
    		MUD_getIndVarMean(fHandle, i, &PPG_2e_RFTuningFrequency);
    		MUD_getIndVarStddev(fHandle, i, &PPG_2e_RFTuningFrequencyError);
    		MUD_getIndVarSkewness(fHandle, i, &PPG_2e_RFTuningFrequencySkewness);
    		MUD_getIndVarHigh(fHandle, i, &PPG_2e_RFTuningFrequencyMax);
    		MUD_getIndVarLow(fHandle, i, &PPG_2e_RFTuningFrequencyMin);
    	} // RF TUNING FREQUENCY
			
			// RF ON DELAYS/DWELLTIMES
    	if ( strcmp(Name,"PPG/PPG2e/num RF on delays (dwell times)") == 0 )
    	{
    		MUD_getIndVarMean(fHandle, i, &PPG_2e_RFOnDelays);
    		MUD_getIndVarStddev(fHandle, i, &PPG_2e_RFOnDelaysError);
    		MUD_getIndVarSkewness(fHandle, i, &PPG_2e_RFOnDelaysSkewness);
    		MUD_getIndVarHigh(fHandle, i, &PPG_2e_RFOnDelaysMax);
    		MUD_getIndVarLow(fHandle, i, &PPG_2e_RFOnDelaysMin);
    	} // RF ON DELAYS/DWELLTIMES
			
			// BEAM OFF TIME
    	if ( strcmp(Name,"PPG/PPG2e/beam off time (ms)") == 0 )
    	{
    		MUD_getIndVarMean(fHandle, i, &PPG_2e_BeamOffTime);
    		MUD_getIndVarStddev(fHandle, i, &PPG_2e_BeamOffTimeError);
    		MUD_getIndVarSkewness(fHandle, i, &PPG_2e_BeamOffTimeSkewness);
    		MUD_getIndVarHigh(fHandle, i, &PPG_2e_BeamOffTimeMax);
    		MUD_getIndVarLow(fHandle, i, &PPG_2e_BeamOffTimeMin);
    	} // BEAM OFF TIME
			
			// DWELLTIMES PER FREQUENCY
    	if ( strcmp(Name,"PPG/PPG2e/Number dwelltimes per freq") == 0 )
    	{
    		MUD_getIndVarMean(fHandle, i, &PPG_2e_DwelltimesPerFrequency);
    		MUD_getIndVarStddev(fHandle, i, &PPG_2e_DwelltimesPerFrequencyError);
    		MUD_getIndVarSkewness(fHandle, i, &PPG_2e_DwelltimesPerFrequencySkewness);
    		MUD_getIndVarHigh(fHandle, i, &PPG_2e_DwelltimesPerFrequencyMax);
    		MUD_getIndVarLow(fHandle, i, &PPG_2e_DwelltimesPerFrequencyMin);
    	} // DWELLTIMES PER FREQUENCY
			
			// POST RF BEAM ON DWELLTIMES
    	if ( strcmp(Name,"PPG/PPG2e/num post RF beamOn dwelltimes") == 0 )
    	{
    		MUD_getIndVarMean(fHandle, i, &PPG_2e_PostRFBeamOnDwelltimes);
    		MUD_getIndVarStddev(fHandle, i, &PPG_2e_PostRFBeamOnDwelltimesError);
    		MUD_getIndVarSkewness(fHandle, i, &PPG_2e_PostRFBeamOnDwelltimesSkewness);
    		MUD_getIndVarHigh(fHandle, i, &PPG_2e_PostRFBeamOnDwelltimesMax);
    		MUD_getIndVarLow(fHandle, i, &PPG_2e_PostRFBeamOnDwelltimesMin);
    	} // POST RF BEAM ON DWELLTIMES
			
			// PSM SCALE FACTOR
    	if ( strcmp(Name,"PPG/PPG2e/psm scaler factor") == 0 or strcmp(Name,"PPG/PPG2e/psm scale factor") == 0 )
    	{
    		MUD_getIndVarMean(fHandle, i, &PPG_2e_PSMScaleFactor);
    		MUD_getIndVarStddev(fHandle, i, &PPG_2e_PSMScaleFactorError);
    		MUD_getIndVarSkewness(fHandle, i, &PPG_2e_PSMScaleFactorSkewness);
    		MUD_getIndVarHigh(fHandle, i, &PPG_2e_PSMScaleFactorMax);
    		MUD_getIndVarLow(fHandle, i, &PPG_2e_PSMScaleFactorMin);
    	} // PSM SCALE FACTOR
    	
			// ENABLE RF 
    	if ( strcmp(Name,"PPG/PPG2e/Enable RF") == 0 or strcmp(Name,"PPG/PPG2e/RF Enabled") == 0 or
    	     strcmp(Name,"PPG/PPG2e/enable RF") == 0 or strcmp(Name,"PPG/PPG2e/RF enabled") == 0  )
    	{	
    		MUD_getIndVarMean(fHandle, i, &PPG_2e_RFEnable);
    		MUD_getIndVarStddev(fHandle, i, &PPG_2e_RFEnableError);
    		MUD_getIndVarSkewness(fHandle, i, &PPG_2e_RFEnableSkewness);
    		MUD_getIndVarHigh(fHandle, i, &PPG_2e_RFEnableMax);
    		MUD_getIndVarLow(fHandle, i, &PPG_2e_RFEnableMin);
    	} // ENABLE RF
			
			// PSM SIMULATED SINGLE TONE
    	if ( strcmp(Name,"PPG/PPG2e/Single tone simulated") == 0 or strcmp(Name,"PPG/PPG2e/single tone simulated") == 0)
    	{
    		MUD_getIndVarMean(fHandle, i, &PPG_2e_SingleToneSimulated);
    		MUD_getIndVarStddev(fHandle, i, &PPG_2e_SingleToneSimulatedError);
    		MUD_getIndVarSkewness(fHandle, i, &PPG_2e_SingleToneSimulatedSkewness);
    		MUD_getIndVarHigh(fHandle, i, &PPG_2e_SingleToneSimulatedMax);
    		MUD_getIndVarLow(fHandle, i, &PPG_2e_SingleToneSimulatedMin);
    	} // PSM SIMULATED SINGLE TONE
			
			
			// ENABLE HELICITY FLIPPING
    	if ( strcmp(Name,"PPG/PPG2e/Enable helicity flipping") == 0 or strcmp(Name,"PPG/PPG2e/enable helicity flipping") == 0)
    	{
    		MUD_getIndVarMean(fHandle, i, &PPG_2e_HelicityFlipEnable);
    		MUD_getIndVarStddev(fHandle, i, &PPG_2e_HelicityFlipEnableError);
    		MUD_getIndVarSkewness(fHandle, i, &PPG_2e_HelicityFlipEnableSkewness);
    		MUD_getIndVarHigh(fHandle, i, &PPG_2e_HelicityFlipEnableMax);
    		MUD_getIndVarLow(fHandle, i, &PPG_2e_HelicityFlipEnableMin);
    	} // ENABLE HELICITY FLIPPING
    	
    	// HELICITY FLIP SLEEP TIME
    	if ( strcmp(Name,"PPG/PPG2e/helicity flip sleep (ms)") == 0 or strcmp(Name,"PPG/PPG2e/Helicity flip sleep (ms)") == 0 )
    	{
    		MUD_getIndVarMean(fHandle, i, &PPG_2e_HelicityFlipSleep);
    		MUD_getIndVarStddev(fHandle, i, &PPG_2e_HelicityFlipSleepError);
    		MUD_getIndVarSkewness(fHandle, i, &PPG_2e_HelicityFlipSleepSkewness);
    		MUD_getIndVarHigh(fHandle, i, &PPG_2e_HelicityFlipSleepMax);
    		MUD_getIndVarLow(fHandle, i, &PPG_2e_HelicityFlipSleepMin);
    	} // HELICITY FLIP SLEEP TIME
   } // end of loop through logged variables
    
   
   // Not sure if this is actually working...
   // Check if the values get read from the .msr file.
   if ( PPG_2e_FrequencyStart == std::numeric_limits<double>::quiet_NaN() or PPG_2e_FrequencyStop == std::numeric_limits<double>::quiet_NaN() or PPG_2e_FrequencyIncrement == std::numeric_limits<double>::quiet_NaN() or PPG_2e_RFOnDelays == std::numeric_limits<double>::quiet_NaN() or PPG_2e_DwelltimesPerFrequency == std::numeric_limits<double>::quiet_NaN() or PPG_2e_PostRFBeamOnDwelltimes == std::numeric_limits<double>::quiet_NaN() or PPG_2e_BeamOffTime == std::numeric_limits<double>::quiet_NaN() )
   {
      std::cout << "Read error: some PPG parameters could not be extracted - exiting." << std::endl;
      
      std::cout << "PPG_2e_FrequencyStart = " << PPG_2e_FrequencyStart << std::endl;
      std::cout << "PPG_2e_FrequencyStop = " << PPG_2e_FrequencyStop << std::endl;
      std::cout << "PPG_2e_FrequencyIncrement = " << PPG_2e_FrequencyIncrement << std::endl;
      std::cout << "PPG_2e_RFOnDelays = " << PPG_2e_RFOnDelays << std::endl;
      std::cout << "PPG_2e_DwelltimesPerFrequency = " << PPG_2e_DwelltimesPerFrequency << std::endl;
      std::cout << "PPG_2e_BeamOffTime = " << PPG_2e_BeamOffTime << std::endl;
      
      return EXIT_FAILURE;
   }
   
   
   unsigned int nHistograms;
   std::vector<unsigned int> nBins;
   MUD_getHists( fHandle, &pType, &nHistograms );
   std::cout << "Number of histograms = " << nHistograms << "\n";
   
   // skip the zero index histogram... it contains
   
   //std::vector<std::vector<unsigned int>> h_vec;
   nBins.resize(nHistograms+1);
   //h_vec.resize(nHistograms+1);
   
   
   //  vectors to hold the *raw* histograms
   std::vector<unsigned int> vB_p;
   std::vector<unsigned int> vB_m;
   std::vector<unsigned int> vF_p;
   std::vector<unsigned int> vF_m;
   
   std::vector<unsigned int> vL_p;
   std::vector<unsigned int> vL_m;
   std::vector<unsigned int> vR_p;
   std::vector<unsigned int> vR_m;
   /*
   std::vector<unsigned int> vNBMB_p;
   std::vector<unsigned int> vNBMB_m;
   std::vector<unsigned int> vNBMF_p;
   std::vector<unsigned int> vNBMF_m;
   */
   
   
   //std::cout << "h_vec size = " << h_vec.size() << "\n";
   for (unsigned int i = 0; i < nHistograms+1; ++i)
   {
      const int strdim = 128;
      char hName[strdim];\
      MUD_getHistTitle(fHandle, i, hName, strdim);
      MUD_getHistNumBins(fHandle, i, &nBins.at(i) );
      //std::cout << "Bins in Histogram " << i << " (" << hName << ") = " << nBins.at(i) << "\n";
      
      
      //std::cout << "strcmp = " << strcmp(hName,"B+") << "\n";
      
      // FREQUENCY VECTOR
      //if ( strcmp(hName,"Frequency") == 0 or strcmp(hName,"frequency") == 0 )
      //{
      //   Frequency.resize( nBins.at(i) );
      //   MUD_getHistData(fHandle, i, &Frequency.at(0) );
      //}
      
      // NMR VECTORS
      if ( strcmp(hName,"B+") == 0 or strcmp(hName,"b+") == 0 )
      {
         vB_p.resize( nBins.at(i) );
         MUD_getHistData(fHandle, i, &vB_p.at(0) );
      }
      if ( strcmp(hName,"B-") == 0 or strcmp(hName,"b-") == 0 )
      {
         vB_m.resize( nBins.at(i) );
         MUD_getHistData(fHandle, i, &vB_m.at(0) );
      }
      if ( strcmp(hName,"F+") == 0 or strcmp(hName,"f+") == 0 )
      {
         vF_p.resize( nBins.at(i) );
         MUD_getHistData(fHandle, i, &vF_p.at(0) );
      }
      if ( strcmp(hName,"F-") == 0 or strcmp(hName,"f-") == 0 )
      {
         vF_m.resize( nBins.at(i) );
         MUD_getHistData(fHandle, i, &vF_m.at(0) );
      }
      
      // NQR VECTORS
      if ( strcmp(hName,"L+") == 0 or strcmp(hName,"l+") == 0 )
      {
         vL_p.resize( nBins.at(i) );
         MUD_getHistData(fHandle, i, &vL_p.at(0) );
      }
      if ( strcmp(hName,"L-") == 0 or strcmp(hName,"l-") == 0 )
      {
         vL_m.resize( nBins.at(i) );
         MUD_getHistData(fHandle, i, &vL_m.at(0) );
      }
      if ( strcmp(hName,"R+") == 0 or strcmp(hName,"r+") == 0 )
      {
         vR_p.resize( nBins.at(i) );
         MUD_getHistData(fHandle, i, &vR_p.at(0) );
      }
      if ( strcmp(hName,"R-") == 0 or strcmp(hName,"r-") == 0 )
      {
         vR_m.resize( nBins.at(i) );
         MUD_getHistData(fHandle, i, &vR_m.at(0) );
      }
      /*
      // NBM VECTORS
      if ( strcmp(hName,"NBMB+") == 0 or strcmp(hName,"nbmb+") == 0 )
      {
         vNBMB_p.resize( nBins.at(i) );
         MUD_getHistData(fHandle, i, &vNBMB_p.at(0) );
      }
      if ( strcmp(hName,"NBMB-") == 0 or strcmp(hName,"nbmb-") == 0 )
      {
         vNBMB_m.resize( nBins.at(i) );
         MUD_getHistData(fHandle, i, &vNBMB_m.at(0) );
      }
      if ( strcmp(hName,"NBMF+") == 0 or strcmp(hName,"nbmf+") == 0 )
      {
         vNBMF_p.resize( nBins.at(i) );
         MUD_getHistData(fHandle, i, &vNBMF_p.at(0) );
      }
      if ( strcmp(hName,"NBMF-") == 0 or strcmp(hName,"nbmf-") == 0 )
      {
         vNBMF_m.resize( nBins.at(i) );
         MUD_getHistData(fHandle, i, &vNBMF_m.at(0) );
      }
      */
   }
   
   MUD_closeRead(fHandle);
   //std::system( cleanup.c_str() );
   
   
   
   // make the frequency vector
   std::vector<unsigned int> Frequency;
   double f_tmp = PPG_2e_FrequencyStart;
   while (f_tmp <= PPG_2e_FrequencyStop)
   {
      Frequency.push_back(f_tmp);
      f_tmp += PPG_2e_FrequencyIncrement;
   }
   
   // Print the contents of the frequency container
   //for (auto i: Frequency) std::cout << i << std::endl;
   
   
   
   //unsigned int n_dtpf = PPG_2e_R;
   unsigned int n_frq_bins = 2*PPG_2e_DwelltimesPerFrequency-1;
   //std::cout << n_frq_bins << std::endl;
   
   // 2D Vectors to hold the frequency-ordered histograms
   std::vector<std::vector<double>> B_p;
   std::vector<std::vector<double>> F_p;
   std::vector<std::vector<double>> B_m;
   std::vector<std::vector<double>> F_m;
   
   std::vector<std::vector<double>> L_p;
   std::vector<std::vector<double>> R_p;
   std::vector<std::vector<double>> L_m;
   std::vector<std::vector<double>> R_m;
   /*
   std::vector<std::vector<double>> NBMB_p;
   std::vector<std::vector<double>> NBMF_p;
   std::vector<std::vector<double>> NBMB_m;
   std::vector<std::vector<double>> NBMF_m;
   */
   
   // dummy content vectors - for filling the real 2D vectors
   std::vector<double> dummy_B_p;
   std::vector<double> dummy_F_p;
   std::vector<double> dummy_B_m;
   std::vector<double> dummy_F_m;
   
   std::vector<double> dummy_L_p;
   std::vector<double> dummy_R_p;
   std::vector<double> dummy_L_m;
   std::vector<double> dummy_R_m;
   /*
   std::vector<double> dummy_NBMB_p;
   std::vector<double> dummy_NBMF_p;
   std::vector<double> dummy_NBMB_m;
   std::vector<double> dummy_NBMF_m;
   */
   
   
   unsigned int counter = 0;
   unsigned int start_bin = static_cast<unsigned int>(PPG_2e_RFOnDelays);
   unsigned int end_bin = Frequency.size()*n_frq_bins + start_bin;
   //std::cout << "start - end = " << end_bin - start_bin << std::endl;
   //std::cout << "B_p size = " << vB_p.size() << std::endl;
   
   // sort the histograms by frequency
   for (unsigned int i = start_bin ; i <= end_bin; ++i)
   {
      // b-NMR
      if ( run > 39999 and run < 45000 )
      {
         dummy_B_p.push_back( static_cast<double>(vB_p.at(i)) );
         dummy_F_p.push_back( static_cast<double>(vF_p.at(i)) );
         dummy_B_m.push_back( static_cast<double>(vB_m.at(i)) );
         dummy_F_m.push_back( static_cast<double>(vF_m.at(i)) );
      }
      // b-NQR
      if ( run > 44999 and run < 50000 )
      {
         dummy_L_p.push_back( static_cast<double>(vL_p.at(i)) );
         dummy_R_p.push_back( static_cast<double>(vR_p.at(i)) );
         dummy_L_m.push_back( static_cast<double>(vL_m.at(i)) );
         dummy_R_m.push_back( static_cast<double>(vR_m.at(i)) );
      }
      /*
      // NBM
      dummy_NBMB_p.push_back( static_cast<double>(vNBMB_p.at(i)) );
      dummy_NBMF_p.push_back( static_cast<double>(vNBMF_p.at(i)) );
      dummy_NBMB_m.push_back( static_cast<double>(vNBMB_m.at(i)) );
      dummy_NBMF_m.push_back( static_cast<double>(vNBMF_m.at(i)) );
      */
      counter += 1;
      if ( counter == n_frq_bins )
      {
         // b-NMR
         B_p.push_back( dummy_B_p );
         F_p.push_back( dummy_F_p );
         B_m.push_back( dummy_B_m );
         F_m.push_back( dummy_F_m );
         // b-NQR
         L_p.push_back( dummy_L_p );
         R_p.push_back( dummy_R_p );
         L_m.push_back( dummy_L_m );
         R_m.push_back( dummy_R_m );
         /*
         // NBM
         NBMB_p.push_back( dummy_NBMB_p );
         NBMF_p.push_back( dummy_NBMF_p );
         NBMB_m.push_back( dummy_NBMB_m );
         NBMF_m.push_back( dummy_NBMF_m );
         */
         // clear the dummy vectors
         dummy_B_p.clear();
         dummy_F_p.clear();
         dummy_B_m.clear();
         dummy_F_m.clear();
         dummy_L_p.clear();
         dummy_R_p.clear();
         dummy_L_m.clear();
         dummy_R_m.clear();
         /*
         dummy_NBMB_p.clear();
         dummy_NBMF_p.clear();
         dummy_NBMB_m.clear();
         dummy_NBMF_m.clear();
         */
         // reset the counter
         counter = 0;
      }
   }
   
   /*
   for (std::size_t i = 0; i < Frequency.size(); ++i)
   {
      std::cout << Frequency.at(i) << "\t" << A.at(i).at(0) << "\t"
                                            << A.at(i).at(1) << "\t"
                                            << A.at(i).at(2) << "\t"
                                            << A.at(i).at(3) << "\t"
                                            << A.at(i).at(4) << std::endl;
   }
   */
   
   // 2D vectors to hold asy
   std::vector<std::vector<double>> A_p;
   std::vector<std::vector<double>> A_m;
   std::vector<std::vector<double>> A;
   std::vector<std::vector<double>> dA_p;
   std::vector<std::vector<double>> dA_m;
   std::vector<std::vector<double>> dA;
   /*
   std::vector<std::vector<double>> NBMA_p;
   std::vector<std::vector<double>> NBMA_m;
   std::vector<std::vector<double>> NBMA;
   std::vector<std::vector<double>> dNBMA_p;
   std::vector<std::vector<double>> dNBMA_m;
   std::vector<std::vector<double>> dNBMA;
   */
   // dummy vectors
   std::vector<double> dummy_A_p;
   std::vector<double> dummy_A_m;
   std::vector<double> dummy_A;
   std::vector<double> dummy_dA_p;
   std::vector<double> dummy_dA_m;
   std::vector<double> dummy_dA;
   /*
   std::vector<double> dummy_NBMA_p;
   std::vector<double> dummy_NBMA_m;
   std::vector<double> dummy_NBMA;
   std::vector<double> dummy_dNBMA_p;
   std::vector<double> dummy_dNBMA_m;
   std::vector<double> dummy_dNBMA;
   */
   
   //print the size of the vectors:
   /*
   std::cout << "B_p = " << B_p.size() << ", " << B_p.at(0).size() << std::endl;
   std::cout << "F_p = " << F_p.size() << ", " << F_p.at(0).size() << std::endl;
   std::cout << "B_m = " << B_m.size() << ", " << B_m.at(0).size() << std::endl;
   std::cout << "F_m = " << F_m.size() << ", " << F_m.at(0).size() << std::endl;
   std::cout << "L_p = " << L_p.size() << ", " << L_p.at(0).size() << std::endl;
   std::cout << "R_p = " << R_p.size() << ", " << R_p.at(0).size() << std::endl;
   std::cout << "L_m = " << L_m.size() << ", " << L_m.at(0).size() << std::endl;
   std::cout << "R_m = " << R_m.size() << ", " << R_m.at(0).size() << std::endl;
   std::cout << "NBMB_p = " << NBMB_p.size() << ", " << NBMB_p.at(0).size() << std::endl;
   std::cout << "NBMF_p = " << NBMF_p.size() << ", " << NBMF_p.at(0).size() << std::endl;
   std::cout << "NBMB_m = " << NBMB_m.size() << ", " << NBMB_m.at(0).size() << std::endl;
   std::cout << "NBMF_m = " << NBMF_m.size() << ", " << NBMF_m.at(0).size() << std::endl;
   */
   
   // compute the *raw* asymmetries
   for (std::size_t i = 0; i < Frequency.size(); ++i)
   {
      for (std::size_t j = 0; j < n_frq_bins; ++j)
      {
         // b-NMR
         if ( run > 39999 and run < 45000 )
         {
            // Two-counter asymmetry
            dummy_A_p.push_back( (B_p.at(i).at(j)-F_p.at(i).at(j))/(B_p.at(i).at(j)+F_p.at(i).at(j)) );
            dummy_dA_p.push_back( 2*std::sqrt( B_p.at(i).at(j)*F_p.at(i).at(j) )/std::pow( B_p.at(i).at(j) + F_p.at(i).at(j) ,1.5) );
            // Two-counter asymmetry
            dummy_A_m.push_back( (B_m.at(i).at(j)-F_m.at(i).at(j))/(B_m.at(i).at(j)+F_m.at(i).at(j)) );
            dummy_dA_m.push_back( 2*std::sqrt( B_m.at(i).at(j)*F_m.at(i).at(j) )/std::pow( B_m.at(i).at(j) + F_m.at(i).at(j) ,1.5) );
            // Four-counter asymmetry
            double r_bnmr = (B_p.at(i).at(j)*F_m.at(i).at(j))/(B_m.at(i).at(j)*F_p.at(i).at(j));
            dummy_A.push_back( (std::sqrt(r_bnmr)-1.0)/(std::sqrt(r_bnmr)+1.0) );
            dummy_dA.push_back( std::sqrt(r_bnmr)*std::sqrt( 1.0/B_p.at(i).at(j) + 1.0/B_m.at(i).at(j) + 1.0/F_p.at(i).at(j) + 1.0/F_m.at(i).at(j) )/std::pow(std::sqrt(r_bnmr)+1, 2) );
         }
         // b-NQR
         if ( run > 44999 and run < 50000 )
         {
            // Two-counter asymmetry
            dummy_A_p.push_back( (L_p.at(i).at(j)-R_p.at(i).at(j))/(L_p.at(i).at(j)+R_p.at(i).at(j)) );
            dummy_dA_p.push_back( 2*std::sqrt( L_p.at(i).at(j)*R_p.at(i).at(j) )/std::pow( L_p.at(i).at(j) + R_p.at(i).at(j) ,1.5) );
            // Two-counter asymmetry
            dummy_A_m.push_back( (L_m.at(i).at(j)-R_m.at(i).at(j))/(L_m.at(i).at(j)+R_m.at(i).at(j)) );
            dummy_dA_m.push_back( 2*std::sqrt( L_m.at(i).at(j)*R_m.at(i).at(j) )/std::pow( L_m.at(i).at(j) + R_m.at(i).at(j) ,1.5) );
            // Four-counter asymmetry
            double r_bnqr = (L_p.at(i).at(j)*R_m.at(i).at(j))/(L_m.at(i).at(j)*R_p.at(i).at(j));
            dummy_A.push_back( (std::sqrt(r_bnqr)-1.0)/(std::sqrt(r_bnqr)+1.0) );
            dummy_dA.push_back( std::sqrt(r_bnqr)*std::sqrt( 1.0/L_p.at(i).at(j) + 1.0/L_m.at(i).at(j) + 1.0/R_p.at(i).at(j) + 1.0/R_m.at(i).at(j) )/std::pow(std::sqrt(r_bnqr)+1, 2) );
         }
         /*
         // NBM
         // Two-counter asymmetry
         dummy_NBMA_p.push_back( (NBMB_p.at(i).at(j)-NBMF_p.at(i).at(j))/(NBMB_p.at(i).at(j)+NBMF_p.at(i).at(j)) );
         dummy_dNBMA_p.push_back( 2*std::sqrt( NBMB_p.at(i).at(j)*NBMF_p.at(i).at(j) )/std::pow( NBMB_p.at(i).at(j) + NBMF_p.at(i).at(j) ,1.5) );
         // Two-counter asymmetry
         dummy_NBMA_m.push_back( (NBMB_m.at(i).at(j)-NBMF_m.at(i).at(j))/(NBMB_m.at(i).at(j)+NBMF_m.at(i).at(j)) );
         dummy_dNBMA_m.push_back( 2*std::sqrt( NBMB_m.at(i).at(j)*NBMF_m.at(i).at(j) )/std::pow( NBMB_m.at(i).at(j) + NBMF_m.at(i).at(j) ,1.5) );
         // Four-counter asymmetry
         double r_nbm = (NBMB_p.at(i).at(j)*NBMF_m.at(i).at(j))/(NBMB_m.at(i).at(j)*NBMF_p.at(i).at(j));
         dummy_NBMA.push_back( (std::sqrt(r_nbm)-1.0)/(std::sqrt(r_nbm)+1.0) );
         dummy_dNBMA.push_back( std::sqrt(r_nbm)*std::sqrt( 1.0/NBMB_p.at(i).at(j) + 1.0/NBMB_m.at(i).at(j) + 1.0/NBMF_p.at(i).at(j) + 1.0/NBMF_m.at(i).at(j) )/std::pow(std::sqrt(r_nbm)+1, 2) );
         */
      }
      // push back the asy vectors
      A_p.push_back(dummy_A_p);
      dA_p.push_back(dummy_dA_p);
      A_m.push_back(dummy_A_m);
      dA_m.push_back(dummy_dA_m);
      A.push_back(dummy_A);
      dA.push_back(dummy_dA);
      /*
      NBMA_p.push_back(dummy_NBMA_p);
      dNBMA_p.push_back(dummy_dNBMA_p);
      NBMA_m.push_back(dummy_NBMA_m);
      dNBMA_m.push_back(dummy_dNBMA_m);
      NBMA.push_back(dummy_NBMA);
      dNBMA.push_back(dummy_dNBMA);
      */
      // clear the dummy vectors
      dummy_A_p.clear();
      dummy_dA_p.clear();
      dummy_A_m.clear();
      dummy_dA_m.clear();
      dummy_A.clear();
      dummy_dA.clear();
      /*
      dummy_NBMA_p.clear();
      dummy_dNBMA_p.clear();
      dummy_NBMA_m.clear();
      dummy_dNBMA_m.clear();
      dummy_NBMA.clear();
      dummy_dNBMA.clear();
      */
   }
   
   /*
   // Print Asy to terminal
   std::cout << "This is the raw Asy:" << std::endl;
   for (std::size_t i = 0; i < A.size(); ++i)
   {
      std::cout << Frequency.at(i) << "\t";
      for (std::size_t j = 0; j < A.at(i).size(); ++j)
      {
         std::cout << std::fixed << std::setprecision(9) << A.at(i).at(j) << "\t" << dA.at(i).at(j) << "\t";
      }
      std::cout << "\n";
   }
   */
   /*
   // Print Asy+ to terminal
   std::cout << "This is the raw Asy(+):" << std::endl;
   for (std::size_t i = 0; i < A_p.size(); ++i)
   {
      std::cout << Frequency.at(i) << "\t";
      for (std::size_t j = 0; j < A_p.at(i).size(); ++j)
      {
         std::cout << std::fixed << std::setprecision(9) << A_p.at(i).at(j) << "\t" << dA_p.at(i).at(j) << "\t";
      }
      std::cout << "\n";
   }
   */
   /*
   // Print Asy- to terminal
   std::cout << "This is the raw Asy(-):" << std::endl;
   for (std::size_t i = 0; i < A_m.size(); ++i)
   {
      std::cout << Frequency.at(i) << "\t";
      for (std::size_t j = 0; j < A_m.at(i).size(); ++j)
      {
         std::cout << std::fixed << std::setprecision(9) << A_m.at(i).at(j) << "\t" << dA_m.at(i).at(j) << "\t";
      }
      std::cout << "\n";
   }
   */
   
   std::vector<double>  A_p_differences;
   std::vector<double> dA_p_differences;
   std::vector<double>  A_m_differences;
   std::vector<double> dA_m_differences;
   std::vector<double>  A_differences;
   std::vector<double> dA_differences;
   
   std::vector<double>  A_p_slopes;
   std::vector<double> dA_p_slopes;
   std::vector<double>  A_m_slopes;
   std::vector<double> dA_m_slopes;
   std::vector<double>  A_slopes;
   std::vector<double> dA_slopes;
   
   
   
   
   // make a vector of the times (i.e., bin centers) in ms;
   std::vector<double> time;
   for (std::size_t i = 0; i < n_frq_bins; ++i)
   {
      //time.push_back( PPG_2e_RFOnTime*( i + 0.5 ) ); // low_edge starts at 0.0
      time.push_back( PPG_2e_RFOnTime*( i + 0.5 ) - PPG_2e_RFOnTime*n_frq_bins*0.5 ); // low_edge starts at 0.0
      //std::cout << time.at(i) << std::endl;
   }
   // make RFon bin time = 0?
   
   
   // find the middle  bin
   auto middle = *std::next( time.begin(), time.size()/2.0 ); // returns value at the middle bin
   unsigned int mid_bin = static_cast<unsigned int>( floor( static_cast<double>(n_frq_bins)/2.0 ) );
   //std::cout << "middle = " << middle <<std::endl;
   //std::cout << "mid_bin = " << mid_bin <<std::endl;
   
   // compute the differenced asymmetries via slopes
   // i.e., use wieghted linear least-squares to 
   for (std::size_t i = 0; i < Frequency.size(); ++i)
   {
      // Hold the sums used in the weighted linear least-squares fits.
      // Four-counter asymmetry
      double w_pre = 0.0;
      double wx_pre = 0.0;
      double wy_pre = 0.0;
      double wxy_pre = 0.0;
      double wx2_pre = 0.0;
      double w_post = 0.0;
      double wx_post = 0.0;
      double wy_post = 0.0;
      double wxy_post = 0.0;
      double wx2_post = 0.0;
      
      // Positive Helicity
      double w_pre_A_p = 0.0;
      double wx_pre_A_p = 0.0;
      double wy_pre_A_p = 0.0;
      double wxy_pre_A_p = 0.0;
      double wx2_pre_A_p = 0.0;
      double w_post_A_p = 0.0;
      double wx_post_A_p = 0.0;
      double wy_post_A_p = 0.0;
      double wxy_post_A_p = 0.0;
      double wx2_post_A_p = 0.0;
      
      // Negative Helicity
      double w_pre_A_m = 0.0;
      double wx_pre_A_m = 0.0;
      double wy_pre_A_m = 0.0;
      double wxy_pre_A_m = 0.0;
      double wx2_pre_A_m = 0.0;
      double w_post_A_m = 0.0;
      double wx_post_A_m = 0.0;
      double wy_post_A_m = 0.0;
      double wxy_post_A_m = 0.0;
      double wx2_post_A_m = 0.0;
      
      
      // for the differences
      double PreRF_A = 0.0;
      double PreRF_dA = 0.0;
      double PreRF_A_p = 0.0;
      double PreRF_dA_p = 0.0;
      double PreRF_A_m = 0.0;
      double PreRF_dA_m = 0.0;
      double PostRF_A = 0.0;
      double PostRF_dA = 0.0;
      double PostRF_A_p = 0.0;
      double PostRF_dA_p = 0.0;
      double PostRF_A_m = 0.0;
      double PostRF_dA_m = 0.0;
      
      // first loop for wieghted averages
      for (std::size_t j = 0; j < n_frq_bins; ++j)
      {
         
         if ( n_frq_bins >= 5 )
         {
            // Pre-RF bins
            if ( j < mid_bin )
            {
               // Four-counter asymmetry
               double w = std::pow(dA.at(i).at(j),-2);
               double x = time.at(j);
               double y = A.at(i).at(j);
               w_pre += w;
               wx_pre += w*x;
               wy_pre += w*y;
               wxy_pre += w*x*y;
               wx2_pre += w*x*x;
               // Positive Helicity
               double w_A_p = std::pow(dA_p.at(i).at(j),-2);
               double x_A_p = time.at(j);
               double y_A_p = A_p.at(i).at(j);
               w_pre_A_p += w_A_p;
               wx_pre_A_p += w_A_p*x_A_p;
               wy_pre_A_p += w_A_p*y_A_p;
               wxy_pre_A_p += w_A_p*x_A_p*y_A_p;
               wx2_pre_A_p += w_A_p*x_A_p*x_A_p;
               //
               /*
               std::cout << "w_A_p       = " << w_A_p << std::endl;
               std::cout << "x_A_p       = " << x_A_p << std::endl;
               std::cout << "y_A_p       = " << y_A_p << std::endl;
               std::cout << "w_pre_A_p   = " << w_pre_A_p << std::endl;
               std::cout << "wx_pre_A_p  = " << wx_pre_A_p << std::endl;
               std::cout << "wy_pre_A_p  = " << wy_pre_A_p << std::endl;
               std::cout << "wxy_pre_A_p = " << wxy_pre_A_p << std::endl;
               std::cout << "wx2_pre_A_p = " << wx2_pre_A_p << std::endl;
               */
               // Negative Helicity
               double w_A_m = std::pow(dA_m.at(i).at(j),-2);
               double x_A_m = time.at(j);
               double y_A_m = A_m.at(i).at(j);
               w_pre_A_m += w_A_m;
               wx_pre_A_m += w_A_m*x_A_m;
               wy_pre_A_m += w_A_m*y_A_m;
               wxy_pre_A_m += w_A_m*x_A_m*y_A_m;
               wx2_pre_A_m += w_A_m*x_A_m*x_A_m;
            }
            // Post-RF bins
            if ( j > mid_bin )
            {
               // Four-counter asymmetry
               double w = std::pow(dA.at(i).at(j),-2);
               double x = time.at(j);
               double y = A.at(i).at(j);
               w_post += w;
               wx_post += w*x;
               wy_post += w*y;
               wxy_post += w*x*y;
               wx2_post += w*x*x;
               // Positive Helicity
               double w_A_p = std::pow(dA_p.at(i).at(j),-2);
               double x_A_p = time.at(j);
               double y_A_p = A_p.at(i).at(j);
               w_post_A_p += w_A_p;
               wx_post_A_p += w_A_p*x_A_p;
               wy_post_A_p += w_A_p*y_A_p;
               wxy_post_A_p += w_A_p*x_A_p*y_A_p;
               wx2_post_A_p += w_A_p*x_A_p*x_A_p;
               // Negative Helicity
               double w_A_m = std::pow(dA_m.at(i).at(j),-2);
               double x_A_m = time.at(j);
               double y_A_m = A_m.at(i).at(j);
               w_post_A_m += w_A_m;
               wx_post_A_m += w_A_m*x_A_m;
               wy_post_A_m += w_A_m*y_A_m;
               wxy_post_A_m += w_A_m*x_A_m*y_A_m;
               wx2_post_A_m += w_A_m*x_A_m*x_A_m;
            }
         }
         
         //for the differences
         if ( j == mid_bin - 1 )
         {
            PreRF_A_p = A_p.at(i).at(j);
            PreRF_dA_p = dA_p.at(i).at(j);
            PreRF_A_m = A_m.at(i).at(j);
            PreRF_dA_m = dA_m.at(i).at(j);
            PreRF_A = A.at(i).at(j);
            PreRF_dA = dA.at(i).at(j);
         } // PreRF
         if ( j == mid_bin + 1 )
         {
            PostRF_A_p = A_p.at(i).at(j);
            PostRF_dA_p = dA_p.at(i).at(j);
            PostRF_A_m = A_m.at(i).at(j);
            PostRF_dA_m = dA_m.at(i).at(j);
            PostRF_A = A.at(i).at(j);
            PostRF_dA = dA.at(i).at(j);
         } // PostRF
      }
      
      // Calculate the slopes/intercepts & extrapolate to RFon bin
      if ( n_frq_bins >= 5 )
      {
         // Four-counter asymmetry
         double delta_pre = w_pre*wx2_pre - wx_pre*wx_pre;
         double slope_pre = (w_pre*wxy_pre - wx_pre*wy_pre)/delta_pre;
         double dslope_pre = std::sqrt( w_pre/delta_pre );
         double intercept_pre = (wy_pre*wx2_pre - wx_pre*wxy_pre)/delta_pre;
         double dintercept_pre = std::sqrt( wx2_pre/delta_pre );
         
         double delta_post = w_post*wx2_post - wx_post*wx_post;
         double slope_post = (w_post*wxy_post - wx_post*wy_post)/delta_post;
         double dslope_post = std::sqrt( w_post/delta_post );
         double intercept_post = (wy_post*wx2_post - wx_post*wxy_post)/delta_post;
         double dintercept_post = std::sqrt( wx2_post/delta_post );
         
         //std::cout << Frequency.at(i) << "\t" << (intercept_post + slope_post*middle) << "\t" << (intercept_pre + slope_pre*middle) << std::endl;
         
         double a_slopes = (intercept_post + slope_post*middle) - (intercept_pre + slope_pre*middle);
         double da_slopes = std::sqrt( dintercept_pre*dintercept_pre + dintercept_post*dintercept_post + middle*middle*( dslope_pre*dslope_pre + dslope_post*dslope_post ) );
         
         A_slopes.push_back( a_slopes );
         dA_slopes.push_back( da_slopes );
         
         // Positive Helicity
         double delta_pre_A_p = w_pre_A_p*wx2_pre_A_p - wx_pre_A_p*wx_pre_A_p;
         //std::cout << " delta_pre_A_p = " << delta_pre_A_p << std::endl;
         double slope_pre_A_p = (w_pre_A_p*wxy_pre_A_p - wx_pre_A_p*wy_pre_A_p)/delta_pre_A_p;
         //std::cout << " slope_pre_A_p = " << slope_pre_A_p << std::endl;
         double dslope_pre_A_p = std::sqrt( w_pre_A_p/delta_pre_A_p );
         //std::cout << "dslope_pre_A_p = " << dslope_pre_A_p << std::endl;
         double intercept_pre_A_p = (wy_pre_A_p*wx2_pre_A_p - wx_pre_A_p*wxy_pre_A_p)/delta_pre_A_p;
         //std::cout << " intercept_pre_A_p = " << intercept_pre_A_p << std::endl;
         double dintercept_pre_A_p = std::sqrt( wx2_pre_A_p/delta_pre_A_p );
         //std::cout << "dintercept_pre_A_p = " << dintercept_pre_A_p << std::endl;
         
         double delta_post_A_p = w_post_A_p*wx2_post_A_p - wx_post_A_p*wx_post_A_p;
         //std::cout << " delta_post_A_p = " << delta_post_A_p << std::endl;
         double slope_post_A_p = (w_post_A_p*wxy_post_A_p - wx_post_A_p*wy_post_A_p)/delta_post_A_p;
         //std::cout << " slope_post_A_p = " << slope_post_A_p << std::endl;
         double dslope_post_A_p = std::sqrt( w_post_A_p/delta_post_A_p );
         //std::cout << "dslope_post_A_p = " << dslope_post_A_p << std::endl;
         double intercept_post_A_p = (wy_post_A_p*wx2_post_A_p - wx_post_A_p*wxy_post_A_p)/delta_post_A_p;
         //std::cout << " intercept_post_A_p = " << intercept_post_A_p << std::endl;
         double dintercept_post_A_p = std::sqrt( wx2_post_A_p/delta_post_A_p );
         //std::cout << "dintercept_post_A_p = " << dintercept_post_A_p << std::endl;
         
         //std::cout << Frequency.at(i) << "\t" << (intercept_post_A_p + slope_post_A_p*middle) << "\t" << (intercept_pre_A_p + slope_pre_A_p*middle) << std::endl;
         
         double a_p_slopes = (intercept_post_A_p + slope_post_A_p*middle) - (intercept_pre_A_p + slope_pre_A_p*middle);
         double da_p_slopes = std::sqrt( dintercept_pre_A_p*dintercept_pre_A_p + dintercept_post_A_p*dintercept_post_A_p + middle*middle*( dslope_pre_A_p*dslope_pre_A_p + dslope_post_A_p*dslope_post_A_p ) );
         
         A_p_slopes.push_back( a_p_slopes );
         dA_p_slopes.push_back( da_p_slopes );
         
         // Negative Helicity
         double delta_pre_A_m = w_pre_A_m*wx2_pre_A_m - wx_pre_A_m*wx_pre_A_m;
         double slope_pre_A_m = (w_pre_A_m*wxy_pre_A_m - wx_pre_A_m*wy_pre_A_m)/delta_pre_A_m;
         double dslope_pre_A_m = std::sqrt( w_pre_A_m/delta_pre_A_m );
         double intercept_pre_A_m = (wy_pre_A_m*wx2_pre_A_m - wx_pre_A_m*wxy_pre_A_m)/delta_pre_A_m;
         double dintercept_pre_A_m = std::sqrt( wx2_pre_A_m/delta_pre_A_m );
         
         double delta_post_A_m = w_post_A_m*wx2_post_A_m - wx_post_A_m*wx_post_A_m;
         double slope_post_A_m = (w_post_A_m*wxy_post_A_m - wx_post_A_m*wy_post_A_m)/delta_post_A_m;
         double dslope_post_A_m = std::sqrt( w_post_A_m/delta_post_A_m );
         double intercept_post_A_m = (wy_post_A_m*wx2_post_A_m - wx_post_A_m*wxy_post_A_m)/delta_post_A_m;
         double dintercept_post_A_m = std::sqrt( wx2_post_A_m/delta_post_A_m );
         
         //std::cout << Frequency.at(i) << "\t" << (intercept_post_A_m + slope_post_A_m*middle) << "\t" << (intercept_pre_A_m + slope_pre_A_m*middle) << std::endl;
         
         double a_m_slopes = (intercept_post_A_m + slope_post_A_m*middle) - (intercept_pre_A_m + slope_pre_A_m*middle);
         double da_m_slopes = std::sqrt( dintercept_pre_A_m*dintercept_pre_A_m + dintercept_post_A_m*dintercept_post_A_m + middle*middle*( dslope_pre_A_m*dslope_pre_A_m + dslope_post_A_m*dslope_post_A_m ) );
         
         A_m_slopes.push_back( a_m_slopes );
         dA_m_slopes.push_back( da_m_slopes );
      }
      
      // Calcualate the asymmetry using the Differences
      A_p_differences.push_back( PostRF_A_p - PreRF_A_p );
      dA_p_differences.push_back( std::sqrt( PostRF_dA_p*PostRF_dA_p + PreRF_dA_p*PreRF_dA_p ) );
      A_m_differences.push_back( PostRF_A_m - PreRF_A_m );
      dA_m_differences.push_back( std::sqrt( PostRF_dA_m*PostRF_dA_m + PreRF_dA_m*PreRF_dA_m ) );
      A_differences.push_back( PostRF_A - PreRF_A );
      dA_differences.push_back( std::sqrt( PostRF_dA*PostRF_dA + PreRF_dA*PreRF_dA ) );
      
      
   }
   
   // Print A_slopes- to terminal
   //for (std::size_t i = 0; i < A_slopes.size(); ++i)
   //{
   //   std::cout << Frequency.at(i) << "\t" << A_p_slopes.at(i) << "\t" << dA_slopes.at(i) << std::endl;
   //}
   
   
   
   
   
   
   
   // Fill the 2D histogram.
   double f_bins = Frequency.size();
   double f_le = PPG_2e_FrequencyStart - 0.5*PPG_2e_FrequencyIncrement;
   double f_ue = PPG_2e_FrequencyStop + 0.5*PPG_2e_FrequencyIncrement;
   double t_bins = n_frq_bins;
   double t_le = 0.0;
   double t_ue = n_frq_bins*PPG_2e_RFOnTime*0.001;
   
   //TGraphErrors gA(Frequency.size(),&Frequency.at(0),&A_slopes.at(0),0,&dA_slopes.at(0));
   //gA.SetMarkerStyle(kFullCircle);
   
   //gA.Draw("AP");
   
   
   // Make the histograms
   TH1D h_A_slopes("h_A_slopes","slopes;Frequency (Hz);#DeltaAsymmetry [slopes]", f_bins, f_le, f_ue);
   h_A_slopes.SetMarkerStyle(kFullCircle);
   h_A_slopes.SetMarkerColor(kBlack);
   h_A_slopes.SetLineColor(kBlack);
   
   TH1D h_A_p_slopes("h_A_p_slopes",";Frequency (Hz);#DeltaAsymmetry(+) [slopes]", f_bins, f_le, f_ue);
   h_A_p_slopes.SetMarkerStyle(kFullCircle);
   h_A_p_slopes.SetMarkerColor(kBlue);
   h_A_p_slopes.SetLineColor(kBlue);
   
   TH1D h_A_m_slopes("h_A_m_slopes",";Frequency (Hz);#DeltaAsymmetry(-) [slopes]", f_bins, f_le, f_ue);
   h_A_m_slopes.SetMarkerStyle(kFullCircle);
   h_A_m_slopes.SetMarkerColor(kRed);
   h_A_m_slopes.SetLineColor(kRed);
   
   TH1D h_A_differences("h_A_differences",";Frequency (Hz);#DeltaAsymmetry [diffs.]", f_bins, f_le, f_ue);
   h_A_differences.SetMarkerStyle(kCircle);
   h_A_differences.SetMarkerColor(kBlack);
   h_A_differences.SetLineColor(kBlack);
   
   TH1D h_A_p_differences("h_A_p_differences",";Frequency (Hz);#DeltaAsymmetry(+) [diffs.]", f_bins, f_le, f_ue);
   h_A_p_differences.SetMarkerStyle(kCircle);
   h_A_p_differences.SetMarkerColor(kBlue);
   h_A_p_differences.SetLineColor(kBlue);
   
   TH1D h_A_m_differences("h_A_m_differences",";Frequency (Hz);#DeltaAsymmetry(-) [diffs.]", f_bins, f_le, f_ue);
   h_A_m_differences.SetMarkerStyle(kCircle);
   h_A_m_differences.SetMarkerColor(kRed);
   h_A_m_differences.SetLineColor(kRed);
   
   // Fill them
   for (std::size_t i = 0; i < Frequency.size(); ++i)
   {
      //
      h_A_slopes.SetBinContent(i+1, A_slopes.at(i) );
      h_A_slopes.SetBinError(i+1, dA_slopes.at(i) );
      h_A_p_slopes.SetBinContent(i+1, A_p_slopes.at(i) );
      h_A_p_slopes.SetBinError(i+1, dA_p_slopes.at(i) );
      h_A_m_slopes.SetBinContent(i+1, A_m_slopes.at(i) );
      h_A_m_slopes.SetBinError(i+1, dA_m_slopes.at(i) );
      //
      h_A_differences.SetBinContent(i+1, A_differences.at(i) );
      h_A_differences.SetBinError(i+1, dA_differences.at(i) );
      h_A_p_differences.SetBinContent(i+1, A_p_differences.at(i) );
      h_A_p_differences.SetBinError(i+1, dA_p_differences.at(i) );
      h_A_m_differences.SetBinContent(i+1, A_m_differences.at(i) );
      h_A_m_differences.SetBinError(i+1, dA_m_differences.at(i) );
   }
   
   // Make stacked histograms for both helicities
   THStack hs_slopes("hs_slopes",";Frequency (Hz);#DeltaAsymmetry(#pm) [slopes]");
   hs_slopes.Add(&h_A_p_slopes);
   hs_slopes.Add(&h_A_m_slopes);
   THStack hs_differences("hs_differences",";Frequency (Hz);#DeltaAsymmetry(#pm) [diffs.]");
   hs_differences.Add(&h_A_p_differences);
   hs_differences.Add(&h_A_m_differences);

   // Make 2D histograms of raw asymmetry
   
   TH2D h_A_p("h_A_p",";Frequency (Hz);Time (s);Asymmetry(+)", f_bins, f_le, f_ue, t_bins, t_le, t_ue );
   for (std::size_t i = 0; i < A_p.size(); ++i)
   {
      for (std::size_t j = 0; j < A_p.at(i).size(); ++j)
      {
         h_A_p.SetBinContent(i+1, j+1, A_p.at(i).at(j));
         h_A_p.SetBinError(i+1, j+1, dA_p.at(i).at(j));
      }
   }
   h_A_p.SetMarkerColor(kBlue);
   h_A_p.SetMarkerStyle(kFullCircle);
   h_A_p.SetLineColor(kBlue);
   
   TH2D h_A_m("h_A_m",";Frequency (Hz);Time (s);Asymmetry(-)", f_bins, f_le, f_ue, t_bins, t_le, t_ue );
   for (std::size_t i = 0; i < A_m.size(); ++i)
   {
      for (std::size_t j = 0; j < A_m.at(i).size(); ++j)
      {
         h_A_m.SetBinContent(i+1, j+1, A_m.at(i).at(j));
         h_A_m.SetBinError(i+1, j+1, dA_m.at(i).at(j));
      }
   }
   h_A_m.SetMarkerColor(kRed);
   h_A_m.SetMarkerStyle(kFullCircle);
   h_A_m.SetLineColor(kRed);
   
   
   /*
   // MgO fit function
   TF1 fMgO("fMgO",[&](double *x, double *p){ return p[0]*TMath::CauchyDist(x[0],p[1],p[2]*0.5) + p[3]*TMath::CauchyDist(x[0],p[1],p[4]*0.5); },f_le, f_ue,5);
   fMgO.SetNpx(1000);
   fMgO.SetParName(0,"Amp_n (a.u.)");
   fMgO.SetParName(1,"Res (Hz)");
   fMgO.SetParName(2,"FWHM_n (Hz)");
   fMgO.SetParName(3,"Amp_w (a.u.)");
   fMgO.SetParName(4,"FWHM_w (Hz)");
   
   fMgO.SetParLimits(0,-100,0);
   fMgO.SetParLimits(1,f_le,f_ue);
   fMgO.SetParLimits(2,0,1000);
   fMgO.SetParLimits(3,-500,0);
   fMgO.SetParLimits(4,0,8000);
   
   fMgO.SetParameter(0,-30);
   fMgO.SetParameter(0, h_A_slopes.GetBinCenter(h_A_slopes.GetMinimumBin()) );
   fMgO.SetParameter(2,500);
   fMgO.SetParameter(3,-1000);
   fMgO.SetParameter(4,4000);
   */
   
   // Don't draw the histogram bin widths (looks bad for 2D histograms!)
   gStyle->SetErrorX(0.0);
   
   std::string canvas_title = "bnmr_2e --- Year: " + std::to_string(year) + " Run: " + std::to_string(run);
   TCanvas the_canvas("the_canvas", canvas_title.c_str());
   the_canvas.Divide(3,2);
   the_canvas.cd(1);
   h_A_p.Draw("E");
   the_canvas.cd(2);
   hs_slopes.Draw("E nostack");
   the_canvas.cd(3);
   hs_differences.Draw("E nostack");
   the_canvas.cd(4);
   h_A_m.Draw("E");
   the_canvas.cd(5);
   h_A_slopes.Draw("E");
   the_canvas.cd(6);
   h_A_differences.Draw("E");
   

   // This terminates the running TApplication when ANY canvas window is closed
   // (i.e., exited with "X") - call it before running the TApplication.
   // https://root.cern.ch/phpBB3/viewtopic.php?t=7183
   TQObject::Connect("TCanvas", "Closed()", "TApplication", gApplication, "Terminate()");
   theApp.Run(kTRUE);
   
   
   // optionally save the asymmertry
   std::cout << "\n\nDo you want to save the asymmetry? [(y)es/(n)o]\n\n"; 
   std::string save_the_file;
   std::cin >> save_the_file;
   
   // Save the asymmetry calculated via slopes
   if (save_the_file == "y" or save_the_file == "yes")
   {
      if ( n_frq_bins >= 5 )
      {
         // Save to text file
         std::string outputfilename( std::to_string(year) + "-" + std::to_string(run) + "-slopes.dat" );
         std::ofstream outputfile;
         outputfile.open( outputfilename.c_str() );
         outputfile << "#Frequency(Hz)\tA+(a.u.)\tdA+(a.u.\tA-(a.u.)\tdA-(a.u.)\tA(a.u.)\tdA(a.u.)\n";
         for (std::size_t i = 0; i < Frequency.size(); ++i)
         {
             outputfile << std::fixed << Frequency.at(i) << std::setprecision(9) << "\t" << A_p_slopes.at(i) << "\t" << dA_p_slopes.at(i) << "\t" << A_m_slopes.at(i) << "\t" << dA_m_slopes.at(i) << "\t" << A_slopes.at(i) << "\t" << dA_slopes.at(i) << "\n";
         }
         outputfile.close();
      }
      
      // Save the asymmetry calculated via differences
      std::string outputfilename_d( std::to_string(year) + "-" + std::to_string(run) + "-differences.dat" );
      std::ofstream outputfile_d;
      outputfile_d.open( outputfilename_d.c_str() );
      outputfile_d << "#Frequency(Hz)\tA+(a.u.)\tdA+(a.u.\tA-(a.u.)\tdA-(a.u.)\tA(a.u.)\tdA(a.u.)\n";
      for (std::size_t i = 0; i < Frequency.size(); ++i)
      {
          outputfile_d << std::fixed << Frequency.at(i) << std::setprecision(9) << "\t" << A_p_differences.at(i) << "\t" << dA_p_differences.at(i) << "\t" << A_m_differences.at(i) << "\t" << dA_m_differences.at(i) << "\t" << A_differences.at(i) << "\t" << dA_differences.at(i) << "\n";
      }
      outputfile_d.close();
   } //
   
   // exit
   return EXIT_SUCCESS;
}



// Get MUD data from the web.
void wgetMUD(unsigned long year, unsigned long run)
{
   std::string MUDfile("http://musr.ca/mud/data/");
   if ( run >= 40000 and run <= 44999 and year >= 1999 )
   {
      MUDfile.append("BNMR/" + std::to_string(year) + "/" + "0" + std::to_string(run) + ".msr");
   }
   else if (run >= 45000 and run <= 49999 and year >= 2003 )
   {
      MUDfile.append("BNQR/" + std::to_string(year) + "/" + "0" + std::to_string(run) + ".msr");
   }
   else
   {
      std::cout << "Invalid year/run\n";
   }
   std::string wget("wget --quiet " + MUDfile + "-O " + MUDfile );
   std::system( wget.c_str() );
}

// Set ROOT asthetics
void SetROOTStyle()
{
   gStyle->SetPadTickX(1);             // X-Axis Border Ticks
   gStyle->SetPadTickY(1);             // Y-Axis Border Ticks
   gStyle->SetPadGridX(0);             // X-Axis Major Gridlines
   gStyle->SetPadGridY(0);             // Y-Axis Major Gridlines
   gStyle->SetPadTopMargin(0.10);		// 0.1
   gStyle->SetPadBottomMargin(0.15);	// 0.1
   gStyle->SetPadLeftMargin(0.15);		// 0.1
   gStyle->SetPadRightMargin(0.10);	   // 0.1
   gStyle->SetLabelFont(42,"xyz");		// Axes label fonts
   gStyle->SetTitleFont(42,"xyz");		// Axes title fonts
   gStyle->SetTitleOffset(1.0,"xyz");	// Axes title position offset
   gStyle->SetTitleSize(0.06,"xyz"); 	// Axes title size
   gStyle->SetTitleBorderSize(0);		// Border box around Histogram/Graph title
   gStyle->SetTitleFont(42,"1");		   // Histogram/Graph title font - arg in "" must not be x, y, or z
   gStyle->SetTitleSize(0.06,"1"); 	   // Axes title size
   gStyle->SetHistMinimumZero(kTRUE);	// Zooms in on all points
   gStyle->SetStripDecimals(kFALSE);   // Keep Constant Decimals
   gStyle->SetOptStat(0);              // Options: ksiourmen 111111110
   gStyle->SetOptFit(0);               // 
   gStyle->SetOptTitle(1);	            // Display title
   
   // Get the resolution of the current display and use it to determine an ideal size of the canvas.
   // http://superuser.com/a/338697
   std::string get_width = "xrandr --current | grep '*' | uniq | awk '{print $1}' | cut -d 'x' -f1";
   std::string get_height = "xrandr --current | grep '*' | uniq | awk '{print $1}' | cut -d 'x' -f2";
   double screen_height = 768;
   FILE *pin = popen(get_height.c_str(), "r");
	char buff[2048];
	if ( pin != 0 )
	{
	   //while( fgets(buff, sizeof(buff), pin) != NULL )
	   //{
      //   std::cout << ;
      //}
      fgets(buff, sizeof(buff), pin);
      screen_height = std::atof(buff);
   }
   pclose(pin);
   
   // Canvas dimensions (4:3)
   double width_to_height_aspect_ratio = 5.5/3.0;
   double fraction_of_screen = 0.8;
   double height = screen_height*fraction_of_screen;
   double width = height*width_to_height_aspect_ratio;
   
   gStyle->SetCanvasDefH(height);      // Canvas height (def. 500)	
   gStyle->SetCanvasDefW(width);       // Canvas width (def. 700)
   gStyle->SetCanvasDefX(0);           // Canvas position on monitor
   gStyle->SetCanvasDefY(0);           // Canvas position on monitor
   //gStyle->SetPaperSize(21.6,27.9);  // Size in cm - US Letter Paper
   gStyle->SetPalette(57);          // Colour Palette (def. kBird = 57)
   gStyle->SetNumberContours(99);      // Colour Contours (def. 20) [between 0 & 1000]
   gStyle->SetColorModelPS(0);         // RGB=0, CMYK=1
   gStyle->SetLineScalePS(3);          // Line scaling for Postscript output (3-5 seems OK)
}
