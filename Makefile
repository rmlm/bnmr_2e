TARGET     = bnmr_2e
INSTALLDIR = 
INCLUDEDIR = `root-config --incdir`
CXX        = g++
CXXFLAGS   = -O2 -Wall -std=c++14 -pthread -I$(INCLUDEDIR)
LDFLAGS    = `root-config --ldflags`
LDLIBS     = `root-config --glibs` -lmud
RM         = rm -f
SOURCES    = $(shell find . -name "*.cpp")
OBJECTS    = $(patsubst %.cpp, %.o, $(SOURCES))

all: $(TARGET)

$(TARGET): $(OBJECTS)
	$(CXX) $(CXXFLAGS) $(LDFLAGS) -o $(TARGET) $(OBJECTS) $(LDLIBS) 

depend: .depend

.depend: $(SOURCES)
	$(RM) *~ .depend
	$(CXX) $(CXXFLAGS) -MM $^>>./.depend;

install:
	mkdir -p $(INSTALLDIR)
	mv -p $(TARGET) $(INSTALLDIR)

clean:
	$(RM) $(OBJECTS)
	
cleaner: clean
	$(RM) *~ .depend

cleanest: cleaner
	$(RM) $(TARGET)

include .depend
